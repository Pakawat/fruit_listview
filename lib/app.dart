
import 'package:flutter/material.dart';
import 'pages/home_page.dart';

class App extends StatelessWidget {
  const App({super.key});


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fruit Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
          // colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          // useMaterial3: true,
      ),
      home: const HomePage(title: 'Fruit Lists'),
    );
  }
}